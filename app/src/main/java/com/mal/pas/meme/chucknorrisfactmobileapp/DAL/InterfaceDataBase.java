package com.mal.pas.meme.chucknorrisfactmobileapp.DAL;

import com.mal.pas.meme.chucknorrisfactmobileapp.BO.BO_chuck_Norris.factClass;

public interface InterfaceDataBase {
    //CRUD
    public factClass createFact(factClass fc);
    public factClass readFact(int id);
    public factClass updateFact(factClass fc);
    public boolean deleteFact(int id);
}