package com.mal.pas.meme.chucknorrisfactmobileapp.Adaptator;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mal.pas.meme.chucknorrisfactmobileapp.BO.BO_chuck_Norris.factClass;
import com.mal.pas.meme.chucknorrisfactmobileapp.R;

import java.util.List;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private List<factClass> mData;
    private LayoutInflater mInflater;

    // data is passed into the constructor
    public MyRecyclerViewAdapter(Context context, List<factClass> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.content_main, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        factClass objectToRecycleView = mData.get(position);
        holder.myTextViewAuthor.setText(objectToRecycleView.getNameAuthor());
        holder.myTextViewDate.setText(objectToRecycleView.getdate());
        holder.myTextViewFact.setText(objectToRecycleView.getFact());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView myTextViewAuthor;
        TextView myTextViewFact;
        TextView myTextViewDate;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}