package com.mal.pas.meme.chucknorrisfactmobileapp.BO.BO_chuck_Norris;

import android.media.Image;

import java.sql.Date;

public class factImageClass {

    private String title;
    private Image factImage;
    private int nbrPepeolLike;
    private int nbrStar;
    private int id;
    private Date datePublished;
    private String nameAuthor;
    private String uriImage;

    public factImageClass() {}

    public factImageClass(String title, Image factImage, int nbrPepeolLike, int nbrStar, int id, Date datePublished, String nameAuthor, String uriImage) {
        this.title = title;
        this.factImage = factImage;
        this.nbrPepeolLike = nbrPepeolLike;
        this.nbrStar = nbrStar;
        this.id = id;
        this.datePublished = datePublished;
        this.nameAuthor = nameAuthor;
        this.uriImage = uriImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Image getFactImage() {
        return factImage;
    }

    public void setFactImage(Image factImage) {
        this.factImage = factImage;
    }

    public int getNbrPepeolLike() {
        return nbrPepeolLike;
    }

    public void setNbrPepeolLike(int nbrPepeolLike) {
        this.nbrPepeolLike = nbrPepeolLike;
    }

    public int getNbrStar() {
        return nbrStar;
    }

    public void setNbrStar(int nbrStar) {
        this.nbrStar = nbrStar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public String getUriImage() {
        return uriImage;
    }

    public void setUriImage(String uriImage) {
        this.uriImage = uriImage;
    }
}
