package com.mal.pas.meme.chucknorrisfactmobileapp.DAL.driverDBClass;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mal.pas.meme.chucknorrisfactmobileapp.BO.BO_chuck_Norris.factClass;
import com.mal.pas.meme.chucknorrisfactmobileapp.DAL.InterfaceAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Parameter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class apiWebDriverService implements InterfaceAPI {

    private static final apiWebDriverService ourInstance = new apiWebDriverService();

    public static apiWebDriverService getInstance() {
        return ourInstance;
    }

    private Gson gson = new Gson();

    private apiWebDriverService() {
    }


    @Override
    public factClass readAleaFact(int nbrFact) {
        return null;
    }

    @Override
    public factClass readRecentFact(int nbrFact) {
        return null;
    }

    @Override
    public factClass readBestFact(int nbrFact) throws ExecutionException, InterruptedException {
        Log.i("Test request", "test function tread");
        AsyncTask<String, Integer, String> result = new GetUrlContentTask().execute("http://www.chucknorrisfacts.fr/api/get");
        Log.i("Test request", result.get().toString());
        return null;
    }

    public List<factClass> testReadBestFact(int nbrFact) throws ExecutionException, InterruptedException {
        AsyncTask<String, Integer,String> result = new GetUrlContentTask().execute("http://www.chucknorrisfacts.fr/api/get");
        List<factClass> fact = (gson.fromJson(result.get(), new TypeToken<List<factClass>>(){}.getType()));
        return fact;
    }

    private class GetUrlContentTask extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            URL url = null;
            String line = "";
            ArrayList<String> resultArrayJsonString = new ArrayList<>();
            try {
                url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                line = rd.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return line;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            // this is executed on the main thread after the process is over
            // update your UI here
            //displayMessage(result);
        }
    }
}
