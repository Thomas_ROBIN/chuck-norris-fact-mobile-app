package com.mal.pas.meme.chucknorrisfactmobileapp.DAL;

import com.mal.pas.meme.chucknorrisfactmobileapp.BO.BO_chuck_Norris.factClass;

import java.util.concurrent.ExecutionException;

public interface InterfaceAPI{
    //R
    public factClass readAleaFact(int nbrFact);
    public factClass readRecentFact(int nbrFact);
    public factClass readBestFact(int nbrFact) throws ExecutionException, InterruptedException;
}

