package com.mal.pas.meme.chucknorrisfactmobileapp.BO.BO_chuck_Norris;

import java.sql.Date;

public class factClass {
    private String title;
    private String fact;
    private int points;
    private int vote;
    private int id;
    private String date;
    private String nameAuthor;

    public factClass() {}

    public factClass(String title, String fact, int points, int vote, int id, String date) {
        this.title = title;
        this.fact = fact;
        this.points = points;
        this.vote = vote;
        this.id = id;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFact() {
        return fact;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }

    public int getpoints() {
        return points;
    }

    public void setpoints(int points) {
        this.points = points;
    }

    public int getvote() {
        return vote;
    }

    public void setvote(int vote) {
        this.vote = vote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getdate() {
        return date;
    }

    public void setdate(String date) {
        this.date = date;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    @Override
    public String toString() {
        return "factClass{" +
                "title='" + title + '\'' +
                ", fact='" + fact + '\'' +
                ", points=" + points +
                ", vote=" + vote +
                ", id=" + id +
                ", date=" + date +
                ", nameAuthor='" + nameAuthor + '\'' +
                '}';
    }
}
