package com.mal.pas.meme.chucknorrisfactmobileapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mal.pas.meme.chucknorrisfactmobileapp.Adaptator.MyRecyclerViewAdapter;
import com.mal.pas.meme.chucknorrisfactmobileapp.BO.BO_chuck_Norris.factClass;
import com.mal.pas.meme.chucknorrisfactmobileapp.DAL.driverDBClass.apiWebDriverService;
import com.mal.pas.meme.chucknorrisfactmobileapp.R;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.app_logo_round);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        FrameLayout factLayout =  (FrameLayout) findViewById(R.id.factView);
        FrameLayout imageFactLayout =  (FrameLayout) findViewById(R.id.imageFactView);
        FrameLayout saveFactLayout =  (FrameLayout) findViewById(R.id.saveFactVew);
        FrameLayout backgroundLayout =  (FrameLayout) findViewById(R.id.backgroundView);
        int id = item.getItemId();

        if (id == R.id.nav_facts) {
            factLayout.setVisibility(View.VISIBLE);
            imageFactLayout.setVisibility(View.INVISIBLE);
            saveFactLayout.setVisibility(View.INVISIBLE);
            backgroundLayout.setVisibility(View.INVISIBLE);
            try {
                List<factClass> factChuck = apiWebDriverService.getInstance().testReadBestFact(10);
                if(null != factChuck){
                    RecyclerView RecyclerViewListFact = (RecyclerView) findViewById(R.id.RecyclerViewListFact);
                    RecyclerViewListFact.setLayoutManager(new LinearLayoutManager(this));
                    MyRecyclerViewAdapter adapter = new MyRecyclerViewAdapter(this, factChuck);
                    RecyclerViewListFact.setAdapter(adapter);
                    //RecyclerViewListFact.set
                    //Toast.makeText(MainActivity.this, factChuck, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "fact isn't downloaded, an error occured", Toast.LENGTH_LONG).show();
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if (id == R.id.nav_pictures) {
            factLayout.setVisibility(View.INVISIBLE);
            imageFactLayout.setVisibility(View.VISIBLE);
            saveFactLayout.setVisibility(View.INVISIBLE);
            backgroundLayout.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_gallery) {
            factLayout.setVisibility(View.INVISIBLE);
            imageFactLayout.setVisibility(View.INVISIBLE);
            saveFactLayout.setVisibility(View.VISIBLE);
            backgroundLayout.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Regarde cette superbe appli !";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Chuck Norris est partout");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
